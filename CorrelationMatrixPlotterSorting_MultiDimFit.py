import os, sys, re
import ROOT
from ROOT import TCanvas, TFile, TH1F, TH2D, TColor
import printer
from ROOT import gROOT
from array import array


ROOT.gStyle.SetPaintTextFormat("4.2f")
ROOT.gStyle.SetFrameLineWidth(2)
ROOT.gStyle.SetLineWidth(1)
ROOT.gStyle.SetOptStat(0)
gROOT.SetBatch(ROOT.kTRUE)


filename=sys.argv[1]
MeasurementType=sys.argv[2] 
verbose=sys.argv[3]

def add_CMS():
    lowX=0.25
    lowY=0.740
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.15, lowY+0.16, "NDC")
    lumi.SetTextFont(61)
    lumi.SetTextSize(0.05)
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.AddText("CMS")
    return lumi

def add_Preliminary():
    lowX=0.25
    lowY=0.69
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.15, lowY+0.16, "NDC")
    lumi.SetTextFont(52)
    lumi.SetTextSize(0.04)
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.AddText("Preliminary")
    return lumi

def add_lumi():
    lowX=0.65
    lowY=0.82
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.30, lowY+0.16, "NDC")
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.SetTextSize(0.035)
    lumi.SetTextFont (   42 )
    lumi.AddText("138 fb^{-1} (13 TeV)")
    return lumi

#parametersToMeasure=["fa3_ggH","CMS_zz4l_fai1","muV","muf"]
#parametersToMeasure=["CMS_zz4l_fai1","fa3_ggH","muV","muf"]

parametersToMeasureDic = {
    'pth':[
        'r_H_PTH_0_45',
        'r_H_PTH_45_80',
        'r_H_PTH_80_120',
        'r_H_PTH_120_200',
        'r_H_PTH_200_350',
        'r_H_PTH_350_450',
        'r_H_PTH_GT450'
        ],
    'njets':[
        'r_H_NJETS_0',
        'r_H_NJETS_1',
        'r_H_NJETS_2',
        'r_H_NJETS_3',
        'r_H_NJETS_GE4'
    ],
    'ljpt':[
        'r_H_NJETS_0',
        'r_H_LJPT_30_60',
        'r_H_LJPT_60_120',
        'r_H_LJPT_120_200',
        'r_H_LJPT_200_350',
        'r_H_LJPT_GT350',
    ]
}


parametersToMeasure = parametersToMeasureDic[MeasurementType]
nPOI = len(parametersToMeasure)


def main():
    fIn = TFile(filename, 'READ')
    filenameOut = filename+"_ReducedCorrMat.root"
    printer.info("input: "+filename+"\n"+"output: "+filenameOut) #+"\nparametersToMeasure : "+MeasurementType)
    #cofH = fIn.Get("covariance_fit_s")    
    cofH = fIn.Get("h_correlation")    
    # Fill 2D histogram
    hPOI = TH2D("","",nPOI,0,nPOI,nPOI,0,nPOI)
    for par_x in parametersToMeasure:
        for par_y in parametersToMeasure:
            #printer.red(par_x)
            #printer.blue(par_y)
            i, j = parametersToMeasure.index(par_x)+1, parametersToMeasure.index(par_y)+1
            binContent = cofH.GetBinContent(cofH.GetXaxis().FindBin(par_x),cofH.GetYaxis().FindBin(par_y))
            if i>=j: 
                hPOI.SetBinContent(i,j,binContent)

        lableWrite = par_x
        if "r_H_" in lableWrite: lableWrite = lableWrite.replace("r_H_","")
        # Replace NJets labels
        if "NJETS" in lableWrite:
            if "_GE" in lableWrite: lableWrite = lableWrite.replace("_GE"," #geq ")
            else: lableWrite = lableWrite.replace("_", " = ")
            if "NJETS" in lableWrite: lableWrite = lableWrite.replace("NJETS","N_{jets}")
        # Replace Higgs pT labels
        if "PTH" in lableWrite: 
            if "_GT" in lableWrite: lableWrite = lableWrite.replace("_GT"," #geq ")            
            if "PTH" in lableWrite: lableWrite = lableWrite.replace("PTH", "P_{T}^{H}")
            if "{H}_" in lableWrite: 
                lableWrite = lableWrite.replace("{H}_", "{H} [")
                lableWrite += "]"
            if "#geq" not in lableWrite: 
                lableWrite = "%s:%s"%(lableWrite[:lableWrite.rfind("_")],lableWrite[lableWrite.rfind("_")+1:])
        # Replace Leading jet pT labels
        if "LJPT" in lableWrite:
            if "_GT" in lableWrite: lableWrite = lableWrite.replace("_GT"," #geq ")
            if "LJPT" in lableWrite: lableWrite = lableWrite.replace("LJPT", "P_{T}^{jet1}")
            if "{jet1}_" in lableWrite: 
                lableWrite = lableWrite.replace("{jet1}_", "{jet1} [")
                lableWrite += "]"
            if "#geq" not in lableWrite: 
                lableWrite = "%s:%s"%(lableWrite[:lableWrite.rfind("_")],lableWrite[lableWrite.rfind("_")+1:])

        hPOI.SetMarkerSize(1.5)
        hPOI.GetXaxis().SetBinLabel(i,lableWrite)
        hPOI.GetYaxis().SetBinLabel(i,lableWrite)


    ##########################
    ##     Build Canvas     ##
    ##########################
    canvas = ROOT.TCanvas("","",0,0,1000,1000)
    canvas.cd()
    margine = 0.2 # 0.4
    canvas.SetLeftMargin(margine)
    canvas.SetBottomMargin(margine)
    
    palette=[4,0,2]
    paletteArr=array("f", palette)
    #TColor.CreateGradientColorTable(3,)
    #ROOT.gStyle.SetPalette(2)
    #hPOI.GetXaxis().ChangeLabel(-1,30,-1.,-1,-1,-1,"test")
    hPOI.SetMinimum(-1.0)
    hPOI.SetMaximum(+1.0)
    # colors 
    ROOT.gStyle.SetNumberContours(128)
    red = array('d', [0., 1., 1.])
    green = array('d', [0., 1., 0.])
    blue = array('d', [1., 1., 0.])
    points = array('d', [0., 0.5, 1.])
    ROOT.TColor.CreateGradientColorTable(3, points, red, green, blue, 128)
    hPOI.GetXaxis().SetLabelSize(0.05)
    hPOI.GetYaxis().SetLabelSize(0.05)
    hPOI.GetZaxis().SetLabelSize(0.025)
    hPOI.LabelsOption("v")
    hPOI.Draw("COLZTEXT")
    # 
    lumi = add_lumi()
    lumi.Draw("same")
    cms = add_CMS()
    cms.Draw("same")
    prelim = add_Preliminary()
    prelim.Draw("same")

    canvas.Update()
    namepdf=filename[:filename.rfind("/")]+"/correlation_"+MeasurementType+".pdf"
    canvas.SaveAs(namepdf)
    canvas.SaveAs(namepdf.replace("pdf","png"))

    printer.green("\n\nimgcat "+namepdf.replace("pdf","png")+"\n\n")




    fOut = TFile(filenameOut, 'RECREATE')

    fIn.Close()
    fOut.Close()

if __name__ == "__main__":
    main()
