import os, sys, re
import ROOT
from ROOT import TCanvas, TFile, TH1F, TH2D, TColor
import printer
from ROOT import gROOT
from array import array

ROOT.gStyle.SetPaintTextFormat("4.2f")
ROOT.gStyle.SetFrameLineWidth(2)
ROOT.gStyle.SetLineWidth(1)
ROOT.gStyle.SetOptStat(0)
gROOT.SetBatch(ROOT.kTRUE)


filename=sys.argv[1]
verbose=sys.argv[2]

def add_CMS():
    lowX=0.25
    lowY=0.740
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.15, lowY+0.16, "NDC")
    lumi.SetTextFont(61)
    lumi.SetTextSize(0.05)
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.AddText("CMS")
    return lumi

def add_Preliminary():
    lowX=0.25
    lowY=0.69
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.15, lowY+0.16, "NDC")
    lumi.SetTextFont(52)
    lumi.SetTextSize(0.04)
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.AddText("Preliminary")
    return lumi

def add_lumi():
    lowX=0.62
    lowY=0.82
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.30, lowY+0.16, "NDC")
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.SetTextSize(0.035)
    lumi.SetTextFont (   42 )
    lumi.AddText("137.0 fb^{-1} (13 TeV)")
    return lumi

def main():
    fIn = TFile(filename, 'READ')
    filenameOut = filename[filename.rfind("/")+1:-5]+"_copy.root"
    printer.info("input: "+filename+"\n"+"output: "+filenameOut)
    cofH = fIn.Get("covariance_fit_s")
    nBins = cofH.GetNbinsY()
    printer.msg("Total parameter numbers:\t%.f"%(nBins))
    POIdicY = {}
    POIlist = []
    BinXIdxlist = []
    BinYIdxlist = []
    for i in range(0,nBins+1):
        paraName = cofH.GetXaxis().GetBinLabel(i)
        if "r_H" in paraName:
            if "v" in verbose: printer.gray("%s\t%.f"%(paraName,i))
            #nPOI = nPOI+1
            POIlist.append(paraName)
            BinXIdxlist.append(i)
    nPOI = len(POIlist)
    printer.red("Total POI numbers:\t%.f"%(nPOI))

    for i in range(0,nBins):        
        paraName = cofH.GetYaxis().GetBinLabel(i)        
        if paraName in POIlist: 
            POIdicY[paraName]=i
            printer.msg("%s\t%.f"%(paraName,i))


    hPOI = TH2D("","",nPOI,0,nPOI,nPOI,0,nPOI)
    for i in range(1,nPOI+1):
        if "v" in verbose: printer.green("%.f, %.f"%(BinXIdxlist[i-1], POIdicY[POIlist[i-1]]))
        for j in range(1,nPOI+1):        
            #print i, "\t", j
            if "v" in verbose: printer.gray("%.f, %.f"%(BinXIdxlist[i-1], POIdicY[POIlist[j-1]]))            
            binContent = cofH.GetBinContent(BinXIdxlist[i-1],POIdicY[POIlist[j-1]])
            #print BinXIdxlist[i-1], "\t",BinXIdxlist[j-1], "\t", binContent
            
            if i>=j: 
                hPOI.SetBinContent(i,j,binContent)
        #print "\n"

        lableWrite = POIlist[i-1]
        if "r_H_" in lableWrite: lableWrite = lableWrite.replace("r_H_","")
        # Replace NJets labels
        if "NJETS" in lableWrite:
            if "_GE" in lableWrite: lableWrite = lableWrite.replace("_GE"," #geq ")
            else: lableWrite = lableWrite.replace("_", " = ")
            if "NJETS" in lableWrite: lableWrite = lableWrite.replace("NJETS","N_{jets}")
        # Replace Higgs pT labels
        if "PTH" in lableWrite: 
            if "_GT" in lableWrite: lableWrite = lableWrite.replace("_GT"," #geq ")            
            if "PTH" in lableWrite: lableWrite = lableWrite.replace("PTH", "P_{T}^{H}")
            if "{H}_" in lableWrite: 
                lableWrite = lableWrite.replace("{H}_", "{H} [")
                lableWrite += "]"
            if "#geq" not in lableWrite: 
                lableWrite = "%s:%s"%(lableWrite[:lableWrite.rfind("_")],lableWrite[lableWrite.rfind("_")+1:])
            
        #hPOI.GetXaxis().SetBinLabel(i,POIlist[i-1])
        #hPOI.GetYaxis().SetBinLabel(i,POIlist[i-1])
        hPOI.SetMarkerSize(1.5)
        hPOI.GetXaxis().SetBinLabel(i,lableWrite)
        hPOI.GetYaxis().SetBinLabel(i,lableWrite)



    ##########################
    ##     Build Canvas     ##
    ##########################
    canvas = ROOT.TCanvas("","",0,0,1000,1000)
    canvas.cd()
    margine = 0.2 # 0.4
    canvas.SetLeftMargin(margine)
    canvas.SetBottomMargin(margine)
    
    palette=[4,0,2]
    paletteArr=array("f", palette)
    #TColor.CreateGradientColorTable(3,)
    #ROOT.gStyle.SetPalette(2)
    #hPOI.GetXaxis().ChangeLabel(-1,30,-1.,-1,-1,-1,"test")
    hPOI.SetMinimum(-1.0)
    hPOI.SetMaximum(+1.0)
    # colors 
    ROOT.gStyle.SetNumberContours(128)
    red = array('d', [0., 1., 1.])
    green = array('d', [0., 1., 0.])
    blue = array('d', [1., 1., 0.])
    points = array('d', [0., 0.5, 1.])
    ROOT.TColor.CreateGradientColorTable(3, points, red, green, blue, 128)
    hPOI.GetXaxis().SetLabelSize(0.05)
    hPOI.GetYaxis().SetLabelSize(0.05)
    hPOI.GetZaxis().SetLabelSize(0.025)
    hPOI.LabelsOption("v")
    hPOI.Draw("COLZTEXT")
    # 
    lumi = add_lumi()
    lumi.Draw("same")
    cms = add_CMS()
    cms.Draw("same")
    prelim = add_Preliminary()
    prelim.Draw("same")

    canvas.Update()
    namepdf="correlation.pdf"
    canvas.SaveAs(namepdf)
    canvas.SaveAs(namepdf.replace("pdf","png"))




    fOut = TFile(filenameOut, 'RECREATE')

    fIn.Close()
    fOut.Close()

if __name__ == "__main__":
    main()
