import printer
import os, sys
import ROOT as r

def runCmd(cmd):
    printer.gray(cmd)
    os.system(cmd)

def runCmdP(cmd, PrinterMode):
    printer.gray(cmd)
    if PrinterMode is False : os.system(cmd)

def fileOpener(fileName, openOption):
    if r.TFile(fileName, openOption).IsZombie():
        printer.error("There is no %s" % fileName)
        return sys.exit(1)
    else:
        printer.gray("File opend:\t"+fileName)
        file = r.TFile(fileName, openOption)
        return r.TFile(fileName, openOption)

def imgLister(list):
    for l in list:
        printer.green("imgcat "+l)

def cdDir(dir):
    printer.yellow("Where are we???\n"+os.getcwd()+"\n\n")
    printer.gray("cd "+dir)
    if os.path.isdir(dir) is False:
        printer.warning("There is no directory - "+ dir)
        if args.printTrue is False: 
            sys.exit(0)
    else: 
        os.chdir(dir)
    printer.green("Where are we???\n"+os.getcwd()+"\n\n")


def mkDir(dir):
    if not(os.path.isdir(dir)):
        printer.blue("create "+dir)
        os.makedirs(os.path.join(dir))
    else:
        printer.gray(dir+" exist")
