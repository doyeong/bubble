#!/usr/bin/env python
import os, sys, re
import printer

inputDatacardName = sys.argv[1]
year = "UNKNOWN"
if "2016.txt" in inputDatacardName: year = "2016"
if "2017.txt" in inputDatacardName: year = "2017"
if "2018.txt" in inputDatacardName: year = "2018"
f = open(inputDatacardName) #"tt/125/combined.txt.cmb")

process, rate, observation, bin="","","",""
while True:
    line = f.readline()
    if not line: break
    #print(line)
    if "process" in line and "OutsideAcceptance" in line:
        process = line
        #printer.gray(process)
    if "rate" in line:
        rate = line
        #printer.gray(rate)
    #if "bin" in line and "htt_tt" in line and "txt" not in line and bin is "":
    if "bin" in line and "tt_2016" in line and "txt" not in line and bin is "" and ".root" not in line:
        bin = line
        print bin
    if "observation" in line:
        observation = line
    #else:
    #    fo.write(line)
processList = process.split()
#print processList
rateList = rate.split()
observationList = observation.split()
binList = bin.split()
print binList
'''
print len(processList)
print len(rateList)
print len(observationList)
print len(binList)
'''
if len(processList) is not len(rateList): 
    printer.warning("processList length(%s) is different from rateList(%s)!!"%s(str(len(processList)), str(len(rateList))))
    sys.exit(1)

cateIdx=1
cateList = {}

for idx in range(0,len(rateList)):
    #print idx
    if processList[idx]==processList[1]:        
        printer.whiteBlueBold("\n"+binList[cateIdx])
        printer.gray(("%-35s:\t%s")%("observation",observationList[cateIdx]))
        cateList[binList[cateIdx]] = {}
        cateList[binList[cateIdx]]["observation"] = observationList[cateIdx]
        cateIdx = cateIdx+1

    printer.msg(("%-35s:\t%s")%(processList[idx],rateList[idx]))
    # Store yields per category 
    if binList[cateIdx-1] in cateList.keys():
        cateList[binList[cateIdx-1]][processList[idx]] = rateList[idx]

'''
# Compute total yields
total={}
totalVBF={}
for proc in cateList["tt_"+year+"_LowTauPt"]:
    #for proc in cateList["htt_em_1_"+year]:
    totalYield, totalYieldVBF = 0.0, 0.0
    for idx in range(1,len(cateList)+1):
        #print proc
        #print cateList[idx][proc]
        if proc in cateList[binList[idx]]:
            totalYield = totalYield + float(cateList[binList[idx]][proc])
            if "_1_" not in binList[idx] and "_2_" not in binList[idx]: 
                totalYieldVBF = totalYieldVBF + float(cateList[binList[idx]][proc])
    total[proc]=totalYield
    totalVBF[proc]=totalYieldVBF
    #print "\n\n"+str(totalYield)
    #print totalYieldVBF,"\n\n"

#sorted(total.keys())
printer.whiteGreenBold("\nTotal yields")
#for proc in total:    printer.gray(("%-35s:\t%s")%(proc,str(total[proc])))
for proc in sorted(total.keys()):    printer.gray(("%-35s:\t%s")%(proc,str(total[proc])))
printer.whiteRedBold("\nTotal VBF category yields")
for proc in sorted(totalVBF.keys()):    printer.gray(("%-35s:\t%s")%(proc,str(totalVBF[proc])))
'''

print "\n\n\n"

f.close()
