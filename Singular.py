import os, sys, re
import array
import ROOT
from ROOT import TCanvas, TFile, TH1F, TH2F
import printer
from ROOT import gROOT
from ROOT import gDirectory
from ROOT import gPad
from ROOT import TMatrixD
from ROOT import TDecompSVD
import myExcalibur as ex


def getCondNum(vec):
    lenVec = len(vec)
    if vec[lenVec-1]!=0.0:
        printer.whiteBlueBold("Condition number = %f / %f = %f"%(vec[0],vec[lenVec-1],vec[0]/vec[lenVec-1]))
    else:
        printer.whiteBlueBold("Condition number = %f / %f = %f"%(vec[0],vec[lenVec-2],vec[0]/vec[lenVec-2]))

def decompose(mat):
    SVD = TDecompSVD(mat)
    printer.gray(" << Response Matrix >>")
    mat.Print()
    printer.gray(" << Singular Vector >>")
    sigVec = SVD.GetSig()
    sigVec.Print()
    getCondNum(sigVec)

MeasurementTypes = ["HiggsPt", "NJets", "LJPT"]

def main():
    filename = sys.argv[1]
    isVerbose = sys.argv[2]
    printer.gray("Input file:\t"+filename+"\n\n")
    file = TFile(filename, 'READ')
    for MeasurementType in MeasurementTypes:
        printer.blue(MeasurementType)
        HistName = MeasurementType + "_TotlaYield"
        Hist = file.Get(HistName)
        nBinsX, nBinsY = Hist.GetNbinsX()-1, Hist.GetNbinsY()-1 # -1 due to summed yield col, row
        printer.blue("%.0f x %.0f matrix"%(nBinsX, nBinsY))
        A = TMatrixD(nBinsX, nBinsY)

        for nx in range(0,nBinsX):
            for ny in range(0,nBinsY):
                #print "A[%.0f, %.0f"]
                #A[nx, ny] = Hist.GetBinContent(nBinsY-ny, nBinsX-nx)
                #A[nx, ny] = Hist.GetBinContent(nx+1, ny+1)
                #print Hist.GetBinContent(ny+1, nx+1)
                A[nx, ny] = Hist.GetBinContent(ny+1, nx+1)
        decompose(A)

        print "\n\n"

    print "\n\n"

if __name__ == "__main__":
    main()
