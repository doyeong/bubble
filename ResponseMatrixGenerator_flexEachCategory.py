import os, sys, re
import array
import ROOT
from ROOT import TCanvas, TFile, TH1F, TH2F
import printer
from ROOT import gROOT
from ROOT import gDirectory
from ROOT import gPad
import myExcalibur as ex

ROOT.gStyle.SetPaintTextFormat("4.3f")
ROOT.gStyle.SetFrameLineWidth(2)
ROOT.gStyle.SetLineWidth(1)
ROOT.gStyle.SetOptStat(0)
gROOT.SetBatch(ROOT.kTRUE)

imglist=[]

def padMaker(pad):
    pad.SetFillColor(0)
    pad.Draw()
    pad.cd()

def binTEXTLabelMaker(h, BinBoundary, RecoBinBoundary):
    Nbin, RecoNbin = len(BinBoundary), len(RecoBinBoundary)
    if "NJets" in h.GetName():
        for i in range(1,Nbin):
            if BinBoundary[i]==1000:
                h.GetXaxis().SetBinLabel(i, "#geq %.0f"%(BinBoundary[i-1]))
            else:
                h.GetXaxis().SetBinLabel(i, "%.0f"%(BinBoundary[i-1]))
        for i in range(1,RecoNbin):
            if RecoBinBoundary[i]==1000:
                h.GetYaxis().SetBinLabel(i, "#geq %.0f"%(RecoBinBoundary[i-1]))
            else:
                h.GetYaxis().SetBinLabel(i, "%.0f"%(RecoBinBoundary[i-1]))
    else:
        for i in range(1,Nbin):
            if BinBoundary[i]==1000:
                h.GetXaxis().SetBinLabel(i, "[%.0f,#infty)"%(BinBoundary[i-1]))
            elif BinBoundary[i]==10000 or (BinBoundary[i]==30 and BinBoundary[i-1]==0):
                h.GetXaxis().SetBinLabel(i, "0-jet")
            else:
                h.GetXaxis().SetBinLabel(i, "[%.0f,%.0f]"%(BinBoundary[i-1],BinBoundary[i]))
        for i in range(1,RecoNbin):
            if RecoBinBoundary[i]==1000:
                h.GetYaxis().SetBinLabel(i, "[%.0f,#infty)"%(RecoBinBoundary[i-1]))
            elif BinBoundary[i]==10000 or (BinBoundary[i]==30 and BinBoundary[i-1]==0):
                h.GetYaxis().SetBinLabel(i, "0-jet")
            else:
                h.GetYaxis().SetBinLabel(i, "[%.0f,%.0f]"%(RecoBinBoundary[i-1],RecoBinBoundary[i]))
    # for yield matrix
    if Nbin==h.GetNbinsX():
        h.GetXaxis().SetBinLabel(Nbin, "yield")
        h.GetYaxis().SetBinLabel(Nbin, "yield")

def addTag(tagText, lowXOffSet=0.0):
    lowX, lowY = 0.80-lowXOffSet, 0.9
    tag = ROOT.TPaveText(lowX, lowY, lowX+0.03, lowY+0.03, "NDC")
    tag.SetBorderSize(   0 )
    tag.SetFillStyle(    0 )
    tag.SetTextAlign(   12 )
    tag.SetTextColor(    1 )
    tag.SetTextSize(0.03)
    tag.SetTextFont (   42 )
    tag.AddText(tagText)
    return tag

def MakeImage(h, BinBoundary, RecoBinBoundary, year, channel, tag, outputDir, TargetCategory):
    canvas = ROOT.TCanvas("","",0,0,1000,1000)
    canvas.cd()
    margine = 0.16
    canvas.SetLeftMargin(margine)
    canvas.SetBottomMargin(margine)
    binTEXTLabelMaker(h, BinBoundary, RecoBinBoundary)
    h.GetYaxis().SetTitleOffset(2)
    h.SetLineColor(2)
    titlesize = 0.03
    h.GetXaxis().SetLabelSize(titlesize)
    h.GetYaxis().SetLabelSize(titlesize)
    h.Draw("BOXTEXT")
    tagTPave = addTag(tag)
    tagTPave.Draw("SAME")
    tagTPaveTargetCategory = addTag(TargetCategory, 0.64)
    tagTPaveTargetCategory.Draw("SAME")
    canvas.Update()
    namepng = outputDir+"/%s_%.f_%s_%s.png"%(h.GetName(), year, channel, TargetCategory)
    canvas.SaveAs(namepng)
    canvas.SaveAs(namepng.replace("png","pdf"))
    imglist.append(namepng)

def yearDetector(filename):
    year, channel = 9999, "unknown"
    if "2018" in filename: year = 2018
    if "2017" in filename: year = 2017
    if "2016" in filename: year = 2016
    if "tt_Differential" in filename: 
        channel = "tt"
        tag = "%.f #tau#tau"%(year)
    if "mt_Differential" in filename: 
        channel = "mt"
        tag = "%.f #mu#tau"%(year)
    if "et_Differential" in filename: 
        channel = "et"
        tag = "%.f e#tau"%(year)
    if "em_Differential" in filename: 
        channel = "em"
        tag = "%.f e#mu"%(year)

    return year, channel, tag

def main():
    filename = sys.argv[1]
    isVerbose = sys.argv[2]
    TargetCategory = sys.argv[3] # LowTauPt, IntermediateTauPt, HighTauPt
    pureRootfileName = filename[filename.rfind("/")+1:-5]
    year, channel, tag = yearDetector(pureRootfileName)

    # Open file
    fIn = ex.fileOpener(filename, 'READ')
    outputDir = "AN-20-022/Figures/ResponseMatrices"
    if not(os.path.isdir(outputDir)):
        os.makedirs(os.path.join(outputDir))

    filenameOut = outputDir+"/"+pureRootfileName+"_ResponseMatrix.root"
    printer.gray("channel: %s, \t year: %.f"%(channel, year))
    printer.gray("Create\t"+filenameOut)
    fOut = TFile(filenameOut, 'RECREATE')

    # List of measured variables
    #vars = ["NJets", "LJPT"]
    vars = ["HiggsPt"]
    #vars = ["NJets"]
    #vars = ["LJPT"]
    varsMap = {"HiggsPt":"PTH", "NJets":"NJ", "LJPT":"J1PT"}
    # List of signal
    signals = ["ggH_htt125", "xH_htt125"]

    nonfidIntegral,totalnonfidIntegral,totalfidIntegral = "",0.0,0.0
    # Loop over variables
    for var in vars:
        printer.info("\n\n"+var+"\t response matrices is making...")
        if "HiggsPt" in var:
            xlable, ylable = "p_{T;gen}^{H} [GeV]", "p_{T;reco}^{H} [GeV]"
            BinBoundary = [0,45,80,120,200,350,450,1000]
            if channel=="tt":
                if "High" in TargetCategory:
                    RecoBinBoundary = [0,80,120,200,350,450,1000]
                else:
                    RecoBinBoundary = [0,45,80,120,200,350,450,1000]
            elif channel=="et":
                RecoBinBoundary = [0,45,80,120,200,350,1000]
            else:
                RecoBinBoundary = BinBoundary
        elif "LJPT" in var:
            xlable, ylable = "p_{T;gen}^{jet1} [GeV]", "p_{T;reco}^{jet1} [GeV]"            
            BinBoundary = [0,30,60,120,200,350,1000] #if channel!="tt" else [30,60,120,200,350,1000,10000]
            RecoBinBoundary = BinBoundary
        elif "NJets" in var:
            xlable, ylable = "N_{jet;gen}", "N_{jet;reco}"
            BinBoundary = [0,1,2,3,4,1000]
            RecoBinBoundary = BinBoundary
        # Number of variable slices
        NSlice, RecoNSlice = len(BinBoundary)-1, len(RecoBinBoundary)-1
        NbinMTT = 9
        isSymmGenAndRecoBins = False # Default is False to don't use this option
        # Loop over tau pT categories
        for dir in fIn.GetListOfKeys():        
            tdirName = dir.GetName()
            if TargetCategory not in tdirName: continue
            if var not in tdirName: 
                if varsMap[var] not in tdirName:
                    continue
            fOut.mkdir(tdirName)
            tdirOut = fOut.Get(tdirName)
            printer.whiteBlueBold(">> "+tdirName)
            tdir = fIn.Get(tdirName)
            # Non-fid 
            #for sig in signals:
            if var==vars[0]:
                nonfidHistoName = "OutsideAcceptance" #sig.replace("_htt125","_htt_nonfid125")
                nonfidHisto = tdir.Get(nonfidHistoName)
                nonfidIntegral += "%s/%s\t%.4f\n"%(tdirName,nonfidHistoName,nonfidHisto.Integral())
                totalnonfidIntegral += nonfidHisto.Integral()
            #printer.red(str(nonfidHisto.Integral()))
            # Histograms for yield table
            hYield=TH2F(tdirName+"_yield", tdirName, NSlice+1,0,NSlice+1,NSlice+1,0,NSlice+1)
            hYield.GetXaxis().SetTitle(xlable)
            hYield.GetYaxis().SetTitle(ylable)
            # Loop over gen bin
            for i in range(0,NSlice):
                printer.yellow( "-------------------------------------------->> gen slice %.f"%(i) )
                for sig in signals:
                    fidBin = "_PTH_%.f_%.f"%(BinBoundary[i], BinBoundary[i+1]) if i<NSlice-1 else "_PTH_GT%.f"%(BinBoundary[i])
                    if "LJPT" in var: 
                        fidBin = fidBin.replace("PTH","J1PT")
                        if "0_30" in fidBin: fidBin = "_NJ_0"
                    if "NJets" in var:
                        if channel=="tt":
                            fidBin = "_NJ_%.f"%(BinBoundary[i]) if i<NSlice-1 else "_NJ_GE%.f"%(BinBoundary[i])
                        else:
                            fidBin = "_NJ_%.f"%(BinBoundary[i]) if i<NSlice-1 else "_NJ_GE%.f"%(BinBoundary[i])
                        
                    histoName = sig.replace("_htt125",fidBin)                    
                    if tdir.Get(histoName):
                        histo = tdir.Get(histoName)                        
                        totalHistoYield = histo.Integral()
                        printer.gray(histoName+"\t"+str(tdir.Get(histoName).Integral()))
                        # Reco Bin Boundaries
                        TrueRecoNSlice = histo.GetNbinsX()/NbinMTT
                        isSymmGenAndRecoBins = True if histo.GetNbinsX() == NbinMTT*NSlice else False
                        # Loop over reco bin
                        accumHistoYield = 0.0
                        passMerging = False
                        for j in range(0,RecoNSlice):
                            realIdxLow, realIdxHigh = j, j
                            # Exception begin
                            if isSymmGenAndRecoBins is True:
                                if BinBoundary[j+1]!=RecoBinBoundary[j+1]:
                                    if passMerging is False: 
                                        if "v" in isVerbose: printer.whiteRedBold("isSymmGenAndRecoBins")                                
                                        passMerging = True
                                        realIdxHigh = j+1
                                    else:
                                        if "v" in isVerbose: printer.whiteGreenBold("isSymmGenAndRecoBins")                                
                                        realIdxLow, realIdxHigh = j+1, j+1
                            # Exception end
                            lowEdge, highEdge = realIdxLow*NbinMTT+1, (realIdxHigh*NbinMTT)+NbinMTT                           
                            if var=="LJPT" and channel=="tt": # rearrange recobin 0-j
                                if j==0:
                                    lowEdge=NbinMTT*(RecoNSlice-1)+1
                                    highEdge=NbinMTT*RecoNSlice
                                else:
                                    lowEdge-=NbinMTT
                                    highEdge-=NbinMTT

                            if "v" in isVerbose:
                                print "lowEdge: %-2.f  highEdge: %-2.f\tyield: %f"%(lowEdge, highEdge, tdir.Get(histoName).Integral(lowEdge, highEdge))
                                print i+1, "\t", j+1
                            binYield = histo.Integral(lowEdge, highEdge)
                            print "Reco %s [%.f-%.f]\t %f"%(var, RecoBinBoundary[j], RecoBinBoundary[j+1], binYield)
                            accumHistoYield += binYield

                            if "v" in isVerbose: printer.green(str(hYield.GetBinContent(i+1,j+1)))
                            hYield.SetBinContent(i+1,j+1, hYield.GetBinContent(i+1,j+1)+binYield)
                            if "v" in isVerbose: printer.red(str(hYield.GetBinContent(i+1,j+1)))
                        if totalHistoYield!=accumHistoYield:
                            printer.warning("totalHistoYield != accumHistoYield \t\t%s"%(tdirName))
                            printer.blue("totalHistoYield: %f\t accumHistoYield: %f"%(totalHistoYield, accumHistoYield))
                            sys.exit(0)
                    else:
                        printer.red(histoName+" is not in "+tdirName)
            tdirOut.cd()
            hYield.SetTitle("Yields")
            hYield.Write(tdirName+"_YieldMatrix")



        printer.red("\n\nMerge histograms of all categories")
        fOut.cd()
        if "v" in isVerbose: print(fOut.ls(),"\n\n\n")
        # Get histogram names in each category
        listYieldMatrices = []
        for cat in fOut.GetListOfKeys():
            catName = cat.GetName()
            if var in catName or varsMap[var] in catName:
                printer.blue(catName)
                hYieldName = "%s/%s_YieldMatrix"%(catName,catName)
                print fOut.Get(hYieldName).Integral()
                listYieldMatrices.append(hYieldName)
        if "v" in isVerbose: print "listYieldMatrices\t",listYieldMatrices

        # Merge all yield histograms
        totalYieldName = var+"_TotlaYield"
        for hy in listYieldMatrices:
            if listYieldMatrices[0] == hy:
                hTotalYield = fOut.Get(hy).Clone(totalYieldName)
            else:
                hTotalYield.Add(fOut.Get(hy),1)
            if "v" in isVerbose: print "hTotalYield.Integral()\t",hTotalYield.Integral()            
            if hy==listYieldMatrices[-1]: totalfidIntegral = hTotalYield.Integral()
            if listYieldMatrices[-1] == hy: 
                printer.red("\n\nFill Response Matrices")

                # Fill summed yield
                for i in range(1,NSlice+1):
                    sumRows = hTotalYield.ProjectionX().GetBinContent(i) # gen sum (sum all rows in ith column)
                    sumCols = hTotalYield.ProjectionY().GetBinContent(i) # reco sum (sum all column in ith row)
                    hTotalYield.SetBinContent(NSlice+1,i, sumCols)
                    hTotalYield.SetBinContent(i,NSlice+1, sumRows)

                    if "v" in isVerbose:
                        printer.blue("Gen %.f bin\t summed yield: %f "%(i,sumRows))
                        printer.green("Reco %.f bin\t summed yield: %f "%(i,sumCols))

                fOut.cd()
                hTotalYield.Write()
                MakeImage(hTotalYield, BinBoundary, RecoBinBoundary, year, channel, tag, outputDir, TargetCategory)
        
                # Make response matrices
                hResponseC=TH2F(var+"_responseC", "Column Unity Normalization Response Matrix", NSlice,0,NSlice,NSlice,0,NSlice)
                hResponseR=TH2F(var+"_responseR", "Row Unity Normalization Response Matrix", NSlice,0,NSlice,NSlice,0,NSlice)
                hResponseC.GetXaxis().SetTitle(xlable)
                hResponseR.GetXaxis().SetTitle(xlable)
                hResponseC.GetYaxis().SetTitle(ylable)
                hResponseR.GetYaxis().SetTitle(ylable)

                # Loop over gen bin
                for i in range(1,NSlice+1):
                   # Loop over reco bin 
                    for j in range(1,RecoNSlice+1):
                        if hTotalYield.GetBinContent(i,NSlice+1)!=0:
                            hResponseC.SetBinContent(i,j, hTotalYield.GetBinContent(i,j)/hTotalYield.GetBinContent(i,NSlice+1))
                        else:
                            hResponseC.SetBinContent(i,j,0)
                        if hTotalYield.GetBinContent(NSlice+1,j)!=0:
                            hResponseR.SetBinContent(i,j, hTotalYield.GetBinContent(i,j)/hTotalYield.GetBinContent(NSlice+1,j))
                        else:
                            hResponseR.SetBinContent(i,j,0)
                fOut.cd()
                hResponseC.Write()
                hResponseR.Write()
                MakeImage(hResponseC, BinBoundary, RecoBinBoundary, year, channel, tag, outputDir, TargetCategory)
                MakeImage(hResponseR, BinBoundary, RecoBinBoundary, year, channel, tag, outputDir, TargetCategory)
    print "\n\n"
    ex.imgLister(imglist)
    print "\n\n"
    if "v" in isVerbose: print nonfidIntegral
    printer.blue("fiducial/(fiducial+non-fiducial) = %.4f/(%.4f+%.4f) = %.4f"%(totalfidIntegral,totalfidIntegral,totalnonfidIntegral,totalfidIntegral/(totalfidIntegral+totalnonfidIntegral)))
    print "\n\n"
if __name__ == "__main__":
    main()
