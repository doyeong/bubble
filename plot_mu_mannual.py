import os,sys
import array
import numpy as np
import argparse
import ROOT
import uproot
from ROOT import TFile
from ROOT import gROOT
gROOT.SetBatch(ROOT.kTRUE)

parser = argparse.ArgumentParser(description = "DeltaScan ")
parser.add_argument('--MeasurementType',nargs = '?', choices=['mjj','pth','njets','ljpt'],help="Specify the kind of differential measurement to make",required=True)
parser.add_argument('--Tag',nargs = 1,help="Output tag: HTT_Output/Output_?",required=True)

args = parser.parse_args()

parametersToMeasureDic = {
    'pth':[
        'r_H_PTH_0_45',
        'r_H_PTH_45_80',
        'r_H_PTH_80_120',
        'r_H_PTH_120_200',
        'r_H_PTH_200_350',
        'r_H_PTH_350_450',
        'r_H_PTH_GT450'
        ],
    'njets':[
        'r_H_NJETS_0',
        'r_H_NJETS_1',
        'r_H_NJETS_2',
        'r_H_NJETS_3',
        'r_H_NJETS_GE4'
    ],
    'ljpt':[
        'r_H_NJETS_0',
        'r_H_LJPT_30_60',
        'r_H_LJPT_60_120',
        'r_H_LJPT_120_200',
        'r_H_LJPT_200_350',
        'r_H_LJPT_GT350',
    ]
}
parametersTagDic = {
    'pth':'Higgs p_{T} [GeV]',
    'njets':'Jet multiplicity',
    'ljpt':'Leading jet p_{T} [GeV]',
}

uncSize = {
    'pth':11.0,
    'njets':0.12,
    'ljpt':10.0
}

delta = {
    'pth':'1.85',
    'njets':'1.35',
    'ljpt':'2.35'
}

signalStrengthsObs = {
    'pth':{
        'r_H_PTH_0_45'    :[[+0.35, -0.50, +0.49],[+0.11, -0.44, +0.43]],
        'r_H_PTH_45_80'   :[[-0.40, -0.71, +0.71],[+0.34, -0.45, +0.46]],
        'r_H_PTH_80_120'  :[[+2.00, -0.66, +0.68],[+1.28, -0.40, +0.43]],
        'r_H_PTH_120_200' :[[+0.95, -0.33, +0.34],[+1.18, -0.26, +0.26]],
        'r_H_PTH_200_350' :[[+1.17, -0.37, +0.46],[+1.18, -0.36, +0.41]],
        'r_H_PTH_350_450' :[[+1.79, -0.72, +0.93],[+1.59, -0.58, +0.64]],
        'r_H_PTH_GT450'   :[[+1.82, -0.94, +1.19],[+2.00, -0.85, +1.02]]
    },
    'njets':{
        'r_H_NJETS_0'     :[[+1.26, -0.50, +0.51],[+1.25, -0.48, +0.46]],
        'r_H_NJETS_1'     :[[+0.99, -0.30, +0.30],[+1.00, -0.27, +0.28]],
        'r_H_NJETS_2'     :[[+0.87, -0.25, +0.26],[+0.84, -0.22, +0.22]],
        'r_H_NJETS_3'     :[[+0.48, -0.48, +0.50],[+0.58, -0.36, +0.36]],
        'r_H_NJETS_GE4'   :[[+0.63, -0.50, +0.53],[+0.57, -0.45, +0.46]]
    },
    'ljpt':{
        'r_H_NJETS_0'     :[[+1.20, -0.52, +0.52],[+1.11, -0.50, +0.50]],
        'r_H_LJPT_30_60'  :[[+0.13, -0.46, +0.46],[+0.24, -0.41, +0.42]],
        'r_H_LJPT_60_120' :[[+0.62, -0.29, +0.29],[+0.60, -0.27, +0.27]],
        'r_H_LJPT_120_200':[[+1.15, -0.27, +0.28],[+1.13, -0.25, +0.26]],
        'r_H_LJPT_200_350':[[+1.02, -0.39, +0.35],[+1.06, -0.34, +0.38]],
        'r_H_LJPT_GT350'  :[[+1.32, -0.65, +0.76],[+1.30, -0.63, +0.72]] 
    }
    
}

# Expected
signalStrengthsExp = {
    'pth':{
        'r_H_PTH_0_45'    :[[+1.000, -0.47, +0.48],[+1.000, -0.43, +0.43]],
        'r_H_PTH_45_80'   :[[+1.000, -0.67, +0.68],[+1.000, -0.45, +0.46]],
        'r_H_PTH_80_120'  :[[+1.000, -0.58, +0.59],[+1.000, -0.38, +0.39]],
        'r_H_PTH_120_200' :[[+1.000, -0.31, +0.31],[+1.000, -0.24, +0.24]],
        'r_H_PTH_200_350' :[[+1.000, -0.34, +0.38],[+1.000, -0.32, +0.34]],
        'r_H_PTH_350_450' :[[+1.000, -0.63, +0.74],[+1.000, -0.49, +0.54]],
        'r_H_PTH_GT450'   :[[+1.000, -0.91, +1.05],[+1.000, -0.74, +0.84]]
    },
    'njets':{
        'r_H_NJETS_0'     :[[+1.000, -0.50, +0.51],[+1.000, -0.47, +0.48]],
        'r_H_NJETS_1'     :[[+1.000, -0.31, +0.32],[+1.000, -0.28, +0.28]],
        'r_H_NJETS_2'     :[[+1.000, -0.27, +0.28],[+1.000, -0.23, +0.24]],
        'r_H_NJETS_3'     :[[+1.000, -0.53, +0.55],[+1.000, -0.38, +0.39]],
        'r_H_NJETS_GE4'   :[[+1.000, -0.56, +0.59],[+1.000, -0.49, +0.52]]
    },
    'ljpt':{
        'r_H_NJETS_0'     :[[+1.000, -0.51, +0.52],[+1.000, -0.49, +0.50]],
        'r_H_LJPT_30_60'  :[[+1.000, -0.45, +0.47],[+1.000, -0.40, +0.41]],
        'r_H_LJPT_60_120' :[[+1.000, -0.29, +0.29],[+1.000, -0.27, +0.27]],
        'r_H_LJPT_120_200':[[+1.000, -0.27, +0.28],[+1.000, -0.25, +0.26]],
        'r_H_LJPT_200_350':[[+1.000, -0.35, +0.49],[+1.000, -0.34, +0.37]],
        'r_H_LJPT_GT350'  :[[+1.000, -0.65, +0.74],[+1.000, -0.62, +0.72]] 
    }
    
}


def addCMS():
    lowX, lowY = 0.18, 0.84
    tag = ROOT.TPaveText(lowX, lowY, lowX+0.03, lowY+0.03, "NDC")
    tag.SetBorderSize(   0 )
    tag.SetFillStyle(    0 )
    tag.SetTextAlign(   12 )
    tag.SetTextColor(    1 )
    tag.SetTextSize(0.043)
    tag.SetTextFont (   61 )
    tag.AddText("CMS")
    return tag

def addPreliminary():
    lowX, lowY = 0.18, 0.802
    tag = ROOT.TPaveText(lowX, lowY, lowX+0.03, lowY+0.03, "NDC")
    tag.SetBorderSize(   0 )
    tag.SetFillStyle(    0 )
    tag.SetTextAlign(   12 )
    tag.SetTextColor(    1 )
    tag.SetTextSize(0.034)
    tag.SetTextFont (   52 )
    tag.AddText("Preliminary")
    return tag

def add_lumi():
    lowX=0.70
    lowY=0.82
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.30, lowY+0.16, "NDC")
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.SetTextSize(0.035)
    lumi.SetTextFont (   42 )
    lumi.AddText("138 fb^{-1} (13 TeV)")
    return lumi

def GetMu(path, par): #, cents, downs, ups):
    # Open file
    file = TFile(path,'READ')
    tree = file.Get("limit")
    # Get Mu values and uncertainties
    mu = []
    tree.GetEntry(0)
    mu.append(getattr(tree,par))
    tree.GetEntry(1)
    mu.append(-mu[0] + getattr(tree,par))
    tree.GetEntry(2)
    mu.append(-mu[0] + getattr(tree,par))
    cent, down, up = mu[0], mu[1], mu[2]
    print "%.3f\t %.3f/+%.3f"%(cent, down, up)

    return cent, abs(down), up



def plot_outofrange(gr, col, sty):
    lines = []
    for ip in range(gr.GetN()):
        if gr.GetY()[ip] > ymax:
            line = ROOT.TLine(gr.GetX()[ip], gr.GetY()[ip] - gr.GetErrorYlow(ip), gr.GetX()[ip], ymax)
            line.SetLineColor(col)
            line.SetLineStyle(sty)
            line.SetLineWidth(2)
            line.Draw()
            lines.append(line)


signalStrengths = signalStrengthsExp
if "obs" in args.Tag:    signalStrengths = signalStrengthsObs

print "Start Mu plot making!"
#inputFolder="HTT_Output/Output_%s/"%(args.Tag[0])
inputFolder="/hdfs/store/user/doyeong/CombineCondor/Differential/%s_%s/"%(args.Tag[0],args.MeasurementType)
unreg_path = inputFolder+"higgsCombine%s_%s_idx.MultiDimFit.mH120.root"%(args.Tag[0],args.MeasurementType)
reg_path = unreg_path#.replace("_idx","_reg_idx")
parametersToMeasure = parametersToMeasureDic[args.MeasurementType]
npoi = len(parametersToMeasure)
graph_unreg = ROOT.TGraphAsymmErrors(npoi)
graph_reg = ROOT.TGraphAsymmErrors(npoi)


unreg_cents, unreg_downs, unreg_ups = np.array([]), np.array([]), np.array([])
reg_cents, reg_downs, reg_ups = np.array([]), np.array([]), np.array([])


for par in parametersToMeasure:
    print "\n\n"+par
    x = float(par[par.rfind("_")+1:]) if "G" not in par else float(par[par.rfind("_")+3:])*1.25 
    # Unreg
    unreg_path_idx = unreg_path.replace("idx",str(parametersToMeasure.index(par)))
    #unreg_cent, unreg_down, unreg_up = signalStrengths[args.MeasurementType][par][0][0]#GetMu(unreg_path_idx, par)
    unreg_cent = signalStrengths[args.MeasurementType][par][0][0]
    unreg_down = abs(signalStrengths[args.MeasurementType][par][0][1])
    unreg_up = abs(signalStrengths[args.MeasurementType][par][0][2])
    
    unreg_cents, unreg_downs, unnreg_ups = np.append(unreg_cents, [unreg_cent]), np.append(unreg_downs, [unreg_down]), np.append(unreg_ups, [unreg_up])
    graph_unreg.SetPoint(parametersToMeasure.index(par), x ,unreg_cent)
    graph_unreg.SetPointError(parametersToMeasure.index(par), uncSize[args.MeasurementType], uncSize[args.MeasurementType], unreg_down , unreg_up)
    # Reg
    reg_path_idx = reg_path.replace("idx",str(parametersToMeasure.index(par)))
    #reg_cent, reg_down, reg_up = GetMu(reg_path_idx, par)
    reg_cent = signalStrengths[args.MeasurementType][par][1][0]
    reg_down = abs(signalStrengths[args.MeasurementType][par][1][1])
    reg_up = abs(signalStrengths[args.MeasurementType][par][1][2])
    reg_cents, reg_downs, reg_ups = np.append(reg_cents, [reg_cent]), np.append(reg_downs, [reg_down]), np.append(reg_ups, [reg_up])
    graph_reg.SetPoint(parametersToMeasure.index(par), x ,reg_cent)
    graph_reg.SetPointError(parametersToMeasure.index(par), 0, 0, reg_down ,reg_up)

    print "improvement: %.2f"%((((unreg_up+unreg_down)/(reg_up+reg_down))-1)*100.0)

######################
#    Build canvas    #    
######################

canvas = ROOT.TCanvas('c1', 'c1', 1000, 1000)
canvas.SetRightMargin(0.05)
canvas.SetLeftMargin(0.15)
canvas.SetGrid(False, True)

ymin, ymax = -2., 4.

legend = ROOT.TLegend(0.55, 0.77, 0.95, 0.9)
legend.SetBorderSize(0)
legend.SetFillStyle(0)

graph_unreg.SetLineWidth(0)
graph_unreg.SetMarkerColor(ROOT.kOrange+10)
graph_unreg.SetMarkerStyle(8)
graph_unreg.SetMarkerSize(2)
graph_unreg.SetFillColor(ROOT.kOrange)
graph_unreg.SetFillStyle(3001)
graph_unreg.Draw('AP2')

graph_unreg.SetTitle('     Signal strengths')
graph_unreg.GetYaxis().SetRangeUser(ymin, ymax)
graph_unreg.GetYaxis().SetTitle('#mu')
graph_unreg.GetXaxis().SetTitle(parametersTagDic[args.MeasurementType])
graph_unreg.GetXaxis().SetTitleSize(0.05)
graph_unreg.GetYaxis().SetTitleSize(0.05)
#graph_unreg.GetXaxis().SetLimits(binning[0], binning[-1])
plot_outofrange(graph_unreg, ROOT.kBlue, ROOT.kDashed)

graph_reg.SetLineColor(ROOT.kBlack)
graph_reg.SetLineWidth(2)
graph_reg.SetMarkerColor(ROOT.kBlack)
graph_reg.SetMarkerStyle(8)
graph_reg.SetMarkerSize(0.8)
graph_reg.Draw('P')
plot_outofrange(graph_reg, ROOT.kBlack, ROOT.kSolid)


legend.AddEntry(graph_reg, 'Regularized (#delta='+delta[args.MeasurementType]+')', 'PL')
legend.AddEntry(graph_unreg, 'Unregularized', 'PF')
legend.Draw()

cmsTPave = addCMS()
cmsTPave.Draw("SAME")
prelimTPave = addPreliminary()
prelimTPave.Draw("SAME")
lumi = add_lumi()
lumi.Draw("same")

print "\n\n"    
canvas.Print("plots/mu_"+args.MeasurementType+".pdf")
canvas.Print("plots/mu_"+args.MeasurementType+".png")
