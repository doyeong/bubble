#!/usr/bin/env python
import ROOT
import re, os, sys
import argparse
import myExcalibur as ex
import printer 

parser = argparse.ArgumentParser(description = "DeltaScan for differential XS regularization")
parser.add_argument('--MeasurementType',nargs = '?', choices=['mjj','pth','njets','ljpt'],help="Specify the kind of differential measurement to make",required=True)
parser.add_argument('--Tag',nargs = 1,help="Output tag: HTT_Output/Output_?",required=True)
parser.add_argument('--CreateWorkspace',help="Run text2workspace",action="store_true")
parser.add_argument('--autoMCStat','-a',help="autoMCStat",action="store_true")
parser.add_argument('--Boundary','-b', default=25, help="Boundary",action="store")
parser.add_argument('--PrinterMode','-p',help="Print runcommand",action="store_true")
parser.add_argument('--ProduceJobScript','-j',help="Print runcommand",action="store_true")
parser.add_argument('--workspaceOnly',help='Create the text cards, and workspaces only, and then exit. Do not attempt any fits.',action='store_true')
parser.add_argument('--Unblind',help="Unblind the analysis, and do it for real. BE SURE ABOUT THIS.",action="store_true")
args = parser.parse_args()

if args.ProduceJobScript is True:
    JobScript = "JobScript"+args.Tag[0]+"_"+args.MeasurementType+".sh"
    file = open(JobScript,"w")

delta, step, repeat = 0.1, 0.25, 31

parametersToMeasureDic = {
    'pth':[
        'r_H_PTH_0_45',
        'r_H_PTH_45_80',
        'r_H_PTH_80_120',
        'r_H_PTH_120_200',
        'r_H_PTH_200_350',
        'r_H_PTH_350_450',
        'r_H_PTH_GT450'
        ],
    'njets':[
        'r_H_NJETS_0',
        'r_H_NJETS_1',
        'r_H_NJETS_2',
        'r_H_NJETS_3',
        'r_H_NJETS_GE4'
    ],
    'ljpt':[
        'r_H_NJETS_0',
        'r_H_LJPT_30_60',        
        'r_H_LJPT_60_120',
        'r_H_LJPT_120_200',
        'r_H_LJPT_200_350',
        'r_H_LJPT_GT350',
    ]  
}

deltaDic = {'pth':'1.85', 'njets':'1.35', 'ljpt':'2.35'}

print "Start Delta Scan!"
inputFolder="HTT_Output/Output_%s"%(args.Tag[0])
inputFolderHDFS="/hdfs/store/user/doyeong/CombineCondor/Differential/%s_%s"%(args.Tag[0],args.MeasurementType)
print "Measurement type : %s,\t Tag : %s\n\n"%(args.MeasurementType, inputFolder)
parametersToMeasure = parametersToMeasureDic[args.MeasurementType]
# Check if input folder exist
if not(os.path.isdir(inputFolder)):
    if not(os.path.isdir(inputFolderHDFS)):        
        print "There is no "+inputFolder
        print "There is no "+inputFolderHDFS
        sys.exit(0)
    else:
        inputFolder = inputFolderHDFS

# Copy txt datacards
UnregTxtDatacard = "%s/FinalCard_%s.txt"%(inputFolder, args.Tag[0])  
RegTxtDatacard = UnregTxtDatacard.replace(".txt","_Reg.txt")
if not(os.path.isfile(UnregTxtDatacard)):
    print "There is no "+UnregTxtDatacard
    sys.exit(0)

UnregTxtDatacardFile = open(UnregTxtDatacard, "r")
RegTxtDatacardFile = open(RegTxtDatacard, "w")
while True:
    line = UnregTxtDatacardFile.readline()
    if not line: break
    if "imax" in line: newline = "imax * number of bins\n"
    elif "jmax" in line: newline = "jmax * number of processes minus 1\n"
    elif "kmax" in line: newline = "kmax * number of nuisance parameters\n"
    else: newline = line
    if args.autoMCStat is True: 
        RegTxtDatacardFile.write(newline)
    else:
        if "autoMCStats" not in newline: RegTxtDatacardFile.write(newline)


# Add lines for 
printer.whiteBlueBold( "\nBelow lines are added for regularization")
RegTxtDatacardFile.write("## Below lines are added for regularization")

for idx in range(1,len(parametersToMeasure)-1):    
    i0, i1, i2 = idx-1, idx, idx+1
    if idx==len(parametersToMeasure)-1: i2 = 0

    RegLines = "\nconstr%.0f constr %s+%s-2*%s delta[%s]"%(idx, parametersToMeasure[i0],parametersToMeasure[i2],parametersToMeasure[i1],deltaDic[args.MeasurementType])
    print RegLines
    RegTxtDatacardFile.write(RegLines)
RegTxtDatacardFile.close()

Range = "[1,-{Boundary},{Boundary}]".format(Boundary = args.Boundary)

# Make new workspace
WorkspaceCommand = "text2workspace.py -P HiggsAnalysis.CombinedLimit.PhysicsModel:multiSignalModel "
if args.MeasurementType == "pth":
    WorkspaceCommand += "--PO 'map=.*/.*H.*PTH_0_45.*:r_H_PTH_0_45%s' "%(Range)
    WorkspaceCommand += "--PO 'map=.*/.*H.*PTH_45_80.*:r_H_PTH_45_80%s' "%(Range)
    WorkspaceCommand += "--PO 'map=.*/.*H.*PTH_80_120.*:r_H_PTH_80_120%s' "%(Range)
    WorkspaceCommand += "--PO 'map=.*/.*H.*PTH_120_200.*:r_H_PTH_120_200%s' "%(Range)
    WorkspaceCommand += "--PO 'map=.*/.*H.*PTH_200_350.*:r_H_PTH_200_350%s' "%(Range)
    WorkspaceCommand += "--PO 'map=.*/.*H.*PTH_350_450.*:r_H_PTH_350_450%s' "%(Range)
    WorkspaceCommand += "--PO 'map=.*/.*H.*PTH_G.450.*:r_H_PTH_GT450%s' "%(Range)
elif args.MeasurementType == 'njets':
    WorkspaceCommand += "--PO 'map=.*/.*H.*NJ_0.*:r_H_NJETS_0%s' "%(Range)
    WorkspaceCommand += "--PO 'map=.*/.*H.*NJ_1.*:r_H_NJETS_1%s' "%(Range)
    WorkspaceCommand += "--PO 'map=.*/.*H.*NJ_2.*:r_H_NJETS_2%s' "%(Range)
    WorkspaceCommand += "--PO 'map=.*/.*H.*NJ_3.*:r_H_NJETS_3%s' "%(Range)
    WorkspaceCommand += "--PO 'map=.*/.*H.*NJ_G.4.*:r_H_NJETS_GE4%s' "%(Range)
elif args.MeasurementType == 'ljpt':
    WorkspaceCommand += "--PO 'map=.*/.*H.*J1PT_30_60.*:r_H_LJPT_30_60%s' "%(Range)
    WorkspaceCommand += "--PO 'map=.*/.*H.*J1PT_60_120.*:r_H_LJPT_60_120%s' "%(Range)
    WorkspaceCommand += "--PO 'map=.*/.*H.*J1PT_120_200.*:r_H_LJPT_120_200%s' "%(Range)
    WorkspaceCommand += "--PO 'map=.*/.*H.*J1PT_200_350.*:r_H_LJPT_200_350%s' "%(Range)
    WorkspaceCommand += "--PO 'map=.*/.*H.*J1PT_G.350.*:r_H_LJPT_GT350%s' "%(Range)
    WorkspaceCommand += "--PO 'map=.*/.*H.*NJ_0.*:r_H_NJETS_0%s' "%(Range)
WorkspaceCommand+= RegTxtDatacard+" -o "+RegTxtDatacard.replace("txt","root")+" -m 125"
if args.CreateWorkspace is True : 
    printer.whiteBlueBold("\nMake workspace")
    ex.runCmdP(WorkspaceCommand, False) #if args.workspaceOnly else ex.runCmdP(WorkspaceCommand, args.PrinterMode)

if args.workspaceOnly:
    sys.exit()

# Scan
print "\n\n"
printer.whiteBlueBold("\nMake directories for the outputs")
if (os.path.isdir(inputFolder+"/deltaScan")):
    ex.runCmdP("rm -r "+inputFolder+"/deltaScan",args.PrinterMode)
if args.PrinterMode is False:
    os.makedirs(os.path.join(inputFolder+"/deltaScan"))
    os.makedirs(os.path.join(inputFolder+"/deltaScan/multidimfit"))
    os.makedirs(os.path.join(inputFolder+"/deltaScan/higgsCombine"))
    os.makedirs(os.path.join(inputFolder+"/deltaScan/matrix"))
else:
    ex.runCmdP("mkdir "+inputFolder+"/deltaScan", args.PrinterMode)
    ex.runCmdP("mkdir "+inputFolder+"/deltaScan/multidimfit", args.PrinterMode)
    ex.runCmdP("mkdir "+inputFolder+"/deltaScan/higgsCombine", args.PrinterMode)
    ex.runCmdP("mkdir "+inputFolder+"/deltaScan/matrix", args.PrinterMode)

InitialDir=os.getcwd()
ex.cdDir(inputFolder)
haddFileList="~/Alex-Hadd/ahadd.py -f deltaScan/higgsCombine.root "


printer.whiteBlueBold("\nStart delta scan")
for i in range(1, repeat):
    printer.whiteRedBold(" delta = %.2f "%(delta))
    if args.ProduceJobScript is True: file.write("# delta = %.2f \n"%(delta))
    fitOutputfile = "deltaScan/higgsCombine/higgsCombineTest.MultiDimFit.mH120."+str(i)+".root "
    cmd = "combine -M MultiDimFit "+"FinalCard_%s_Reg.root"%(args.Tag[0])+" -t -1 --setParameters delta=%f --saveFitResult \n\n"%(delta)
    if args.autoMCStat is True:  cmd = cmd.replace("\n\n", "--X-rtd MINIMIZER_analytic\n\n")        
    if args.Unblind is True: cmd = cmd.replace(" -t -1 "," ")
    if args.ProduceJobScript is True: 
        hdfsPath = "/hdfs/store/user/doyeong/CombineCondor/Differential/%s_%s"%(args.Tag[0],args.MeasurementType)
        workspaceInHdfs = hdfsPath+"/FinalCard_%s_Reg.root"%(args.Tag[0])
        file.write(cmd.replace("FinalCard_%s_Reg.root"%(args.Tag[0]), workspaceInHdfs))

    cmd += "mv multidimfit.root deltaScan/multidimfit/multidimfit_"+str(i)+".root\n"
    cmd += "mv higgsCombineTest.MultiDimFit.mH120.root "+fitOutputfile+"\n" 
    cmd += "python "+InitialDir+"/scripts/compute_gcc.py deltaScan/multidimfit/multidimfit_"+str(i)+".root "+fitOutputfile+str(delta)+" "+args.MeasurementType+"\n"
    cmd += "mv test.png deltaScan/matrix/mat_"+str(i)+".png\n\n\n"
    ex.runCmdP(cmd,args.PrinterMode)
    haddFileList += fitOutputfile

    if args.ProduceJobScript is True: 
        jobcmd = "python /hdfs/store/user/doyeong/CombineCondor/Differential/compute_gcc.py multidimfit.root higgsCombineTest.MultiDimFit.mH120.root "+str(delta)+" "+args.MeasurementType+"\n"
        jobcmd += "xrdcp multidimfit.root root://cmsxrootd.hep.wisc.edu/"+hdfsPath[5:]+"/deltaScan/multidimfit/multidimfit_"+str(i)+".root\n"
        jobcmd += "xrdcp higgsCombineTest.MultiDimFit.mH120.root root://cmsxrootd.hep.wisc.edu/"+hdfsPath[5:]+"/deltaScan/"+fitOutputfile+"\n"
        jobcmd += "xrdcp test.png root://cmsxrootd.hep.wisc.edu/"+hdfsPath[5:]+"/deltaScan/matrix/mat_"+str(i)+".png\n"
        file.write(jobcmd+"\n\n\n")
        
    delta += step

printer.whiteBlueBold("\nhadd all outputs and make plot")
cmd = haddFileList+"\n\n"
cmd += "python "+InitialDir+"/scripts/plot_delta_scan.py deltaScan/higgsCombine.root deltaScan/globalCorrelationScan.png\n\n"
ex.runCmdP(cmd,args.PrinterMode)


printer.green( "\n\n"+inputFolder+"/deltaScan/globalCorrelationScan.png is created!\n\n")
if args.ProduceJobScript is True: 
    printer.green(JobScript +" is created! copy workspace to hdfs to use this script.\n")
    printer.red("scp "+inputFolder+"/FinalCard_%s_Reg.root"%(args.Tag[0])+" doyeong@login.hep.wisc.edu:"+workspaceInHdfs+"\n")
    printer.red("scp "+JobScript+" doyeong@login.hep.wisc.edu:"+hdfsPath+"/.\n")    
    printer.red("mkdir "+hdfsPath+"/deltaScan/multidimfit")
    printer.red("mkdir "+hdfsPath+"/deltaScan/higgsCombine")
    printer.red("mkdir "+hdfsPath+"/deltaScan/matrix\n\n")
