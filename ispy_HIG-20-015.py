import FWCore.ParameterSet.Config as cms

process = cms.Process('ISPY')

process.load('Configuration.StandardSequences.GeometryDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

process.GlobalTag.globaltag = '80X_dataRun2_Prompt_v8'

process.source = cms.Source('PoolSource',
                            #fileNames = cms.untracked.vstring('file://pickevents_HTT_BoostedHiggs_HadronicChannel.root'))
                            fileNames = cms.untracked.vstring('file://pickevents.root'))
  
from FWCore.MessageLogger.MessageLogger_cfi import *

process.add_(
    cms.Service('ISpyService',
    outputFileName = cms.untracked.string('miniAOD.ig'),
    outputIg = cms.untracked.bool(True),
    outputMaxEvents = cms.untracked.int32(25), # These are the number of events per ig file 
    debug = cms.untracked.bool(False)
    )
)

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(-1) # These are the number of events to cycle through in the input root file
)

process.load('ISpy.Analyzers.ISpyEvent_cfi')
process.load('ISpy.Analyzers.ISpyEBRecHit_cfi')
process.load('ISpy.Analyzers.ISpyEERecHit_cfi')
process.load('ISpy.Analyzers.ISpyESRecHit_cfi')

process.load('ISpy.Analyzers.ISpyHBRecHit_cfi') #
process.load('ISpy.Analyzers.ISpyHERecHit_cfi') #
process.load('ISpy.Analyzers.ISpyHFRecHit_cfi') #
process.load('ISpy.Analyzers.ISpyHORecHit_cfi') #

process.load('ISpy.Analyzers.ISpyPATMuon_cfi')
process.load('ISpy.Analyzers.ISpyPATElectron_cfi')
process.load('ISpy.Analyzers.ISpyPATJet_cfi')
process.load('ISpy.Analyzers.ISpyPATMET_cfi')
process.load('ISpy.Analyzers.ISpyPATPhoton_cfi')

process.load('ISpy.Analyzers.ISpyPFMET_cfi') #
process.load('ISpy.Analyzers.ISpyCaloTau_cfi')
process.load('ISpy.Analyzers.ISpyPFTau_cfi') #
process.load('ISpy.Analyzers.ISpyPFJet_cfi') #
#process.load('ISpy.Analyzers.ISpyGenJet_cfi')

process.load('ISpy.Analyzers.ISpyPackedCandidate_cfi')
process.load('ISpy.Analyzers.ISpyVertex_cfi')
#process.load('ISpy.Analyzers.ISpyTrack_cfi')

process.load('ISpy.Analyzers.ISpyCaloTau_cfi')
process.load('ISpy.Analyzers.ISpyCaloCluster_cfi')

process.ISpyEBRecHit.iSpyEBRecHitTag = cms.InputTag('reducedEgamma:reducedEBRecHits')
process.ISpyEERecHit.iSpyEERecHitTag = cms.InputTag('reducedEgamma:reducedEERecHits')
process.ISpyESRecHit.iSpyESRecHitTag = cms.InputTag('reducedEgamma:reducedESRecHits')

process.ISpyHBRecHit.iSpyHBRecHitTag = cms.InputTag("reducedHcalRecHits:hbhereco") #
process.ISpyHERecHit.iSpyHERecHitTag = cms.InputTag("reducedHcalRecHits:hbhereco") #
process.ISpyHFRecHit.iSpyHFRecHitTag = cms.InputTag("reducedHcalRecHits:hfreco") #
process.ISpyHORecHit.iSpyHORecHitTag = cms.InputTag("reducedHcalRecHits:horeco") #
process.ISpyHBRecHit.iSpyHBRecHitTag = cms.InputTag('reducedEgamma:reducedHBHEHits')#
process.ISpyHERecHit.iSpyHERecHitTag = cms.InputTag('reducedEgamma:reducedHBHEHits')#

#process.ISpyHBRecHit.iSpyHBRecHitTag = cms.InputTag('hbhereco')
#process.ISpyHERecHit.iSpyHERecHitTag = cms.InputTag('hbhereco')
#
process.ISpyHFRecHit.iSpyHFRecHitTag = cms.InputTag('hfreco')
e#process.ISpyHORecHit.iSpyHORecHitTag = cms.InputTag('horeco')

process.ISpyPATElectron.iSpyPATElectronTag = cms.InputTag('slimmedElectrons')
process.ISpyPATElectron.isAOD = cms.untracked.bool(True)

process.ISpyPackedCandidate.iSpyPackedCandidateTag = cms.InputTag('packedPFCandidates') #

process.ISpyPFTau.iSpyPFTauTag = cms.InputTag("hpsPFTauProducer")
process.ISpyPFTau.iSpyPFTauTag = cms.InputTag('fixedConeHighEffPFTauProducer') #
process.ISpyPFTau.iSpyPFTauTag = cms.InputTag('slimmedTaus') #
process.ISpyCaloTau.iSpyCaloTauTag = cms.InputTag('caloRecoTauProducer') #
process.ISpyPATJet.iSpyPATJetTag = cms.InputTag('slimmedJets')
#process.ISpyPFJet.iSpyPFJetTag = cms.InputTag('ak8PFJetsCHS') #
#process.ISpyGenJet.iSpyGenJetTag = cms.InputTag('ak5PFJets') #
#process.ISpyPFJet.etMin = cms.double(30.0) #
#process.ISpyPFJet.etaMax = cms.double(2.5) #


process.ISpyPATMET.iSpyPATMETTag = cms.InputTag('slimmedMETs')

#process.ISpyPATMuon.iSpyPATMuonTag = cms.InputTag("slimmedMuons")
#process.ISpyPATMuon.isAOD = cms.untracked.bool(True)

#process.ISpyPATPhoton.iSpyPATPhotonTag = cms.InputTag('slimmedPhotons')

process.ISpyVertex.iSpyVertexTag = cms.InputTag('offlineSlimmedPrimaryVertices')
process.ISpyCaloCluster.iSpyCaloClusterTag = cms.InputTag('reducedEgamma:reducedEBEEClusters')
process.ISpyCaloCluster.iSpyCaloClusterTag = cms.InputTag('reducedEgamma:reducedHBHEClusters')
#process.ISpyTrack.iSpyTrackTag = cms.InputTag("generalTracks")
#process.ISpyTrackExtrapolation.trackPtMin = cms.double(2.0)


process.iSpy = cms.Path(process.ISpyEvent*
                        process.ISpyEBRecHit*
                        process.ISpyEERecHit*
                        process.ISpyESRecHit*
                        process.ISpyPATElectron*
                        process.ISpyPATJet*
                        process.ISpyPATMET*
                        process.ISpyPATMuon*
                        process.ISpyPATPhoton*
                        process.ISpyPackedCandidate*
                        process.ISpyPFTau* #
                        process.ISpyPFMET* #
                        process.ISpyPFJet* #
                        #process.ISpyGenJet*
                        process.ISpyHBRecHit* #
                        process.ISpyHERecHit* #
                        process.ISpyHFRecHit* #
                        process.ISpyHORecHit* #
                        process.ISpyCaloTau*
                        process.ISpyCaloCluster* #
                        #process.ISpyTrack* 
                        process.ISpyVertex)

process.schedule = cms.Schedule(process.iSpy)

