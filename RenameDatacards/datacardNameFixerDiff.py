import time
import os, sys, re
import array
import ROOT
from ROOT import TCanvas, TFile, TH1D, TH2D
import printer


def main():
    start = time.time()
    filename = sys.argv[1]
    if "2016" in filename: year = 2016
    if "2017" in filename: year = 2017
    if "2018" in filename: year = 2018

    filenameOut = "../shapes/"+filename #[0:-5]+"_nameFixed.root"
    printer.yellow("Create "+filenameOut)
    fOut = TFile(filenameOut, 'RECREATE')

    if "tt_Differential" in filename: 
        channel = "tt"
    if "mt_Differential" in filename: 
        channel = "mt"
    if "et_Differential" in filename: 
        channel = "et"
    if "em_Differential" in filename: 
        channel = "em"

    categories = [
        'htt_PTH_%s_LowTauPt'%(channel),
        'htt_PTH_%s_IntermediateTauPt'%(channel),
        'htt_PTH_%s_HighTauPt'%(channel),
        'htt_NJ_%s_LowTauPt'%(channel),
        'htt_NJ_%s_IntermediateTauPt'%(channel),
        'htt_NJ_%s_HighTauPt'%(channel),
        'htt_J1PT_%s_LowTauPt'%(channel),
        'htt_J1PT_%s_IntermediateTauPt'%(channel),
        'htt_J1PT_%s_HighTauPt'%(channel)
    ]

    xcategories = [
        'htt_PTH_em',
        'htt_NJ_em',
        'htt_J1PT_em'
    ]


    #for dir in fIn.GetListOfKeys(): 
    #tdirName = dir.GetName() 
    for dir in categories:
        tdirName = dir#.GetName() 
        fIn = TFile(filename, 'READ')    
        tdir = fIn.Get(tdirName)
        printer.whiteBlueBold(">> "+tdirName)        
        # Define reco binning
        if "PTH" in tdirName:
            var = "PTH"
            BinBoundary = [0,45,80,120,200,350,450,1000]
            if channel=="tt":
                if "High" in tdirName:
                    RecoBinBoundary = [0,80,120,200,350,450,1000]
                else:
                    RecoBinBoundary = BinBoundary
            elif channel=="mt" or channel=="et":
                if "High" in tdirName:
                    RecoBinBoundary = BinBoundary
                else:
                    RecoBinBoundary = [0,45,80,120,200,350,1000]
            else:
                RecoBinBoundary = BinBoundary
        elif "J1PT" in tdirName:
            var = "J1PT"
            BinBoundary = [0,30,60,120,200,350,1000] 
            if channel=="tt": # last bin is 0J, and they are empty
                RecoBinBoundary = [30,60,120,200,350,1000,9999]
            else:
                RecoBinBoundary = BinBoundary
        elif "NJ" in tdirName:
            var = "NJ" # although tt has 0J bin, they are empty
            BinBoundary = [0,1,2,3,4,1000]
            RecoBinBoundary = BinBoundary

        cat = tdirName[tdirName.find(var)+len(var):]
        cat = cat.replace("_","")

        newTDirNames = []
        for idx, recobin in enumerate(RecoBinBoundary):            
            if idx < len(RecoBinBoundary)-1:
                if "NJ" in tdirName:
                    newTDirName = "htt_%s_%.f_cat%s%s"%(var, RecoBinBoundary[idx], year, cat)
                    if "_4_" in newTDirName:
                        newTDirName = "htt_%s_GE%.f_cat%s%s"%(var, RecoBinBoundary[idx], year, cat)
                else:
                    newTDirName = "htt_%s_%.f_%.f_cat%s%s"%(var, RecoBinBoundary[idx], RecoBinBoundary[idx+1], year, cat)
                if "1000" in newTDirName:
                    newTDirName = "htt_%s_GT%.f_cat%s%s"%(var, RecoBinBoundary[idx], year, cat)    
                newTDirNames.append(newTDirName)
                fOut.mkdir(newTDirName)
                tdirOut = fOut.Get(newTDirName)


                printer.blue(">> "+newTDirName)

        # Now copy histograms to output file
        for h in tdir.GetListOfKeys():
            hName = h.GetName()
            #if "data_obs" not in hName: continue
            histo = tdir.Get(hName)
            
            if histo.GetNbinsX() != (len(RecoBinBoundary)-1)*9:
                print histo.GetNbinsX()
                print len(RecoBinBoundary)
                printer.warning("Check RecoBinBoundary")
                sys.exit(0)
                
            for idx, newTDir in enumerate(newTDirNames):
                massbin = 9
                h=TH1D(hName, hName, massbin,0,massbin)
                for i in range(massbin):
                    InputBin = idx*massbin + i + 1
                    OutputBin = i + 1
                    h.SetBinContent(OutputBin, histo.GetBinContent(InputBin))
                    h.SetBinError(OutputBin, histo.GetBinError(InputBin))
                    #if i==8: print(histo.GetBinContent(InputBin))
                    #print(InputBin,"  ", histo.GetBinContent(InputBin))
                    #if InputBin==1: print(histo.GetBinContent(InputBin))
                tdirOut = fOut.Get(newTDir)
                tdirOut.cd()
                h.Write()
            
        fIn.Close()
        printer.green("inputfile closed")
        ####
        '''
            if "jetFakes_CMS_FF_closure_jet1pt_tt_pth" in hName:
                histo.SetName((histo.GetName()).replace("jetFakes_CMS_FF_closure_jet1pt_tt_pth", "jetFakes_CMS_FF_closure_jet1pt_pth"))
                tdirOut.cd()
                histo.Write()
                printer.red('\t renaming ... '+histo.GetName()+"\t\t"+str(histo.Integral()))
            else:

    histo.SetName(histo.GetName())
                tdirOut.cd()
                histo.Write()

        fIn.Close()
        printer.green("inputfile closed")
 
        '''

    fOut.Close()
    printer.green("outputfile closed")
    printer.gray("time : %.3fs"%(time.time() - start)  )
    sys.exit(0)



if __name__ == "__main__":
    main()
