#!/usr/bin/env python
import re, os, sys
import printer
import subprocess

def runCmdEnv(cmd, PrinterMode, PrintTrue, Nindent=0):
    indent = ""
    for i in range(Nindent):
        indent += "\t"
    if PrintTrue is True: 
        print (indent+">>> "+cmd+"\n")
    if PrinterMode is False : 
        popen = subprocess.Popen(cmd.split( ), stdout=subprocess.PIPE)
        return popen

def main():
    #cmd = "cat Output_030921_ZjR6Tt/FinalCard_030921_ZjR6Tt.txt" # 1783510
    #cmd = "cat hig-20-015/HiggsPt/FinalCard_200721_aJXdiY.txt" # 1783509.9997

    #cmd = "cat Output_030921_vgBaxy/FinalCard_030921_vgBaxy.txt" # 1796008
    #cmd = "cat hig-20-015/NJets/FinalCard_200721_ycl1lZ.txt" # 1796008.0

    #cmd = "cat hig-20-015/LeadingJetPt/FinalCard_200721_hAdrti.txt" # 1793503.9999
    cmd = "cat Output_030921_Evl1sK/FinalCard_030921_Evl1sK.txt" # 1793504

    obsLine = runCmdEnv(cmd, False, True)
    for line in iter(obsLine.stdout.readline, ''): 
        if "observation" in line:
            #print (line.rstrip())
            obs = line.rsplit()
            total_obs = 0
            for i, o in enumerate(obs):
                if i==0: continue
                if "observation" in o: continue
                print o
                total_obs += float(o)
            printer.blue(str(total_obs))
            break


if __name__ == "__main__":
    main()
