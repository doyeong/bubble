#!/usr/bin/env python
import os, sys, re
import printer

def main():
    path=sys.argv[1]
    file_list = os.listdir(path)
    if "PTH" in path: var = "PTH"
    if "NJ" in path: var = "NJ"
    if "J1PT" in path: var = "J1PT"
    years = ["2016", "2017", "2018"]

    fileSTR = "combineCards.py "
    for year in years:
        printer.whiteBlueBold("\nVar: %s Year: %s"%(var, year))
        for file in file_list:        
            if year not in file: continue
            if "txt" not in file: continue
            if "htt_" not in file: continue

            print file
            fileSTR += " "+file[:-5]+"="+file
    fileSTR += " > FinalCard_htt_%s_Run2_Regularized.txt"%(var)
    printer.gray(fileSTR)
    os.system(fileSTR)
if __name__ == "__main__":
    main()
