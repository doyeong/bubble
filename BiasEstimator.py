#!/usr/bin/env python
import os
import sys
import math
import tempfile
import subprocess
import resource
import ROOT
import numpy as np
import root_numpy as rnp
import printer 
resource.setrlimit(resource.RLIMIT_STACK, (resource.RLIM_INFINITY, resource.RLIM_INFINITY))

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


ws_dir = sys.argv[1] # directory where workspace and other fit result files are
observable = sys.argv[2] # name of the observable
sr = sys.argv[3] # name of the signal region bin to shift (e.g. PTH_0_20)
poi = "r_H_NJETS" if sr=="njets" else "r_H_PTH"
try:
    dnscale = float(sys.argv[4]) # signal content in the bin will be shifted by sqrt(original signal) * dnscale
except IndexError:
    dnscale = 1.




if observable == 'pth':
    delta = '2.1'
    mus = ['r_H_PTH_0_45','r_H_PTH_45_80','r_H_PTH_80_120','r_H_PTH_120_200','r_H_PTH_200_350','r_H_PTH_350_450','r_H_PTH_GT450']
elif observable == 'njets':
    delta = '2.1'
    mus = ['r_H_NJETS_0','r_H_NJETS_1','r_H_NJETS_2','r_H_NJETS_3','r_H_NJETS_GE4']
def get_counts(workspace):
    """
    Add up (over all categories) the integrals of reco-level bin sr
    """

    model = workspace.pdf('model_s')
    x = workspace.var('CMS_th1x')
    xset = ROOT.RooArgSet(x)
    ch = workspace.cat('CMS_channel')
    
    count = 0.
    
    
    for index in range(ch.numBins('')):
        ch.setBin(index)
        bin_name = ch.getLabel() # categories
        printer.msg("bin_name: "+bin_name)
        #if sr not in bin_name: continue   
        printer.gray("bin_name: "+bin_name)     
        func = workspace.arg('prop_bin%s' % bin_name)
        integ = func.createIntegral(xset)
        count += integ.getVal()
    printer.blue("count: %f"%(count))
    return count


# Pick up the best-fit asimov dataset

#combine -M GenerateOnly higgsCombine081020_qYD8YL_njets.MultiDimFit.mH120.root -t -1 --saveToys --setParameters r_H_NJETS_0=1,r_H_NJETS_1=1,r_H_NJETS_2=1,r_H_NJETS_3=1,r_H_NJETS_GE4=1 -n BestFitUnregAsimovBkgOnly --snapshotName MultiDimFit

# combine -M GenerateOnly higgsCombine081020_qYD8YL_njets.MultiDimFit.mH120.root -t -1 --saveToys --expectSignal 1 -n BestFitUnregAsimov --snapshotName MultiDimFit

# combine -M MultiDimFit higgsCombine081020_qYD8YL_njets.MultiDimFit.mH120.root --algo none -t -1 --setParameters regularize=1,delta=9.52 --X-rtd MINIMIZER_analytic --saveFitResult -n AltReg_BestFitUnregAsimov 

#combine -M GenerateOnly /hdfs/store/user/doyeong/CombineCondor/Differential/111020_ZOtXLc_njets/higgsCombine111020_ZOtXLc_njets_0.MultiDimFit.mH120.root -t -1 --saveToys -n BestFitUnregAsimov --snapshotName MultiDimFit --saveWorkspace --setParameters r_H_NJETS_0=1,r_H_NJETS_1=1,r_H_NJETS_2=1,r_H_NJETS_3=1,r_H_NJETS_GE4=1
#combine -M GenerateOnly /hdfs/store/user/doyeong/CombineCondor/Differential/111020_ZOtXLc_njets/higgsCombine111020_ZOtXLc_njets_0.MultiDimFit.mH120.root -t -1 --saveToys -n BestFitUnregAsimovBkgOnly --snapshotName MultiDimFit --saveWorkspace --setParameters r_H_NJETS_0=0,r_H_NJETS_1=0,r_H_NJETS_2=0,r_H_NJETS_3=0,r_H_NJETS_GE4=0
#combineTool.py -M MultiDimFit /hdfs/store/user/doyeong/CombineCondor/Differential/111020_ZOtXLc_njets/Workspace_njets.root --saveWorkspace --robustFit=1 --X-rtd MINIMIZER_analytic --X-rtd FAST_VERTICAL_MORPH --algo=singles --cl=0.68 -n 111020_ZOtXLc_njets_0 --floatOtherPOIs=1 --setParameters r_H_NJETS_0=1,r_H_NJETS_1=1,r_H_NJETS_2=1,r_H_NJETS_3=1,r_H_NJETS_GE4=1  --saveFitResult

path = '%s/higgsCombineBestFitUnregAsimov.GenerateOnly.mH120.123456.root' % ws_dir
source = ROOT.TFile.Open(path)
asimov_s = source.Get('toys/toy_asimov')
source.Close()

path = '%s/higgsCombineBestFitUnregAsimovBkgOnly.GenerateOnly.mH120.123456.root' % ws_dir
#path = '%s/higgsCombineBestFitUnregAsimov.GenerateOnly.mH120.123456.root' % ws_dir
source = ROOT.TFile.Open(path)
asimov_b = source.Get('toys/toy_asimov')
source.Close()

# Signal contribution in the Asimov dataset in the signal region bin sr
n = 0.
print "asimov_s.numEntries():", asimov_s.numEntries()
for nd in range(asimov_s.numEntries()):
    point = asimov_s.get(nd)
    bin_name = point.find('CMS_channel').getLabel()
    #print "bin_name: "+ bin_name
    asimov_b.get(nd)
    #print asimov_s.weight(), "\t" , asimov_b.weight(), "\t", asimov_s.weight() - asimov_b.weight()
    n += asimov_s.weight() - asimov_b.weight()
printer.red("%f"%(n))
# Compute the best-fit (to the Asimov dataset) signal yield

ws_path = '%s/higgsCombineAltReg_BestFitUnregAsimov.MultiDimFit.mH120.root' % ws_dir
#ws_path = '%s/higgsCombine111020_ZOtXLc_njets_0.MultiDimFit.mH120.root' % ws_dir

muhat = np.array(tuple(rnp.root2array(ws_path, 'limit', mus)[0]))
print muhat
source = ROOT.TFile.Open(ws_path)
workspace = source.Get('w')
source.Close()

workspace.loadSnapshot('MultiDimFit')

# integral with best-fit mu values
nuhat = get_counts(workspace)

for mu in mus:
    printer.gray("->")
    print workspace.var(mu).getVal()
    workspace.var(mu).setVal(0.)
    printer.gray("-->")
    print workspace.var(mu).getVal()

# integral with mu = 0
bkghat = get_counts(workspace)

nuhat -= bkghat
printer.yellow("nuhat : %f"%(nuhat))
# Create a new dataset with signal content in one reco-level bin scaled by (1 + sqrt(n) * dnscale / n)

new_data = asimov_s.emptyClone()

dn = math.sqrt(n) * dnscale
reldn = dn / n

for nd in range(asimov_s.numEntries()):
    point_s = asimov_s.get(nd)
    bin_name = point_s.find('CMS_channel').getLabel()
    weight = asimov_s.weight()
    #if sr in bin_name:
    asimov_b.get(nd)
    weight += (weight - asimov_b.weight()) * reldn

    new_data.add(point_s, weight)

with tempfile.NamedTemporaryFile(suffix='.root', delete=False) as tfile:
    tmpname = tfile.name


output = ROOT.TFile.Open(tmpname, 'recreate')
new_data.Write('data_obs')
output.Close()

#cmd = ['combine', '/hdfs/store/user/doyeong/CombineCondor/Differential/111020_ZOtXLc_njets/FinalCard_111020_ZOtXLc_Reg_delta4p6.root']
cmd = ['combine', '/hdfs/store/user/doyeong/CombineCondor/Differential/081020_EGerzU_njets/FinalCard_081020_EGerzU_Reg_delta2p1.root']
#cmd = ['combine', '/hdfs/store/user/doyeong/CombineCondor/Differential/081020_H6qj7Y_pth/FinalCard_081020_H6qj7Y_Reg_delta2p1.root']
cmd += ['-M', 'MultiDimFit', '--algo', 'none']
cmd += ['--setParameters', 'delta=%s' % delta]
cmd += ['--X-rtd', 'MINIMIZER_analytic']
cmd += ['-D', '%s:data_obs' % tmpname]
print (cmd)
proc = subprocess.Popen(cmd)
proc.communicate()

os.unlink(tmpname)

outname = 'higgsCombineTest.MultiDimFit.mH120.root'

# dmu = mu values from the fit to the perturbed dataset - mu values from the fit to the original dataset

dmu = np.array(tuple(rnp.root2array(outname, 'limit', mus)[0])) - muhat

os.unlink(outname)

try:
    os.makedirs('%s/bias' % ws_dir)
except OSError:
    pass

# bias = dmu / dn * (nuhat - n)
# The basic idea is that nuhat should be ~ n if there is no regularization bias
# So nuhat - n is the bias in terms of bin content, and dmu / dn converts that into the bias in terms of mu values
printer.blue("dmu")
print (dmu) #"dmu: %f"%(dmu))
printer.blue("dn")
print (dn) #"dn: %f"%(dn))
printer.blue("nuhat")
print (nuhat) #"nuhat: %f"%(nuhat))
printer.blue("n")
print (n) #"n: %f"%(n))
bias = np.array([tuple(dmu / dn * (nuhat - n))], dtype=[(mu, 'f4') for mu in mus])
idx=0
printer.gray("--- Bias ---")
for b in bias[0]:
    printer.gray("%s\t: %.3f"%(mus[idx],b))
    idx+=1

rnp.array2root(bias, '%s/bias/bias.root' % (ws_dir), 'bias')
